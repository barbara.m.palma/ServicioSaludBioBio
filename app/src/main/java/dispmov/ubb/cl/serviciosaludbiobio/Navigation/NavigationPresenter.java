package dispmov.ubb.cl.serviciosaludbiobio.Navigation;

import android.content.Context;

public class NavigationPresenter implements NavigationContract.Presenter {

    private NavigationContract.Navigator navigator;

    public NavigationPresenter() {
    }

    @Override
    public void setNavigator(Context context) {
        navigator = new NavigationNavigator(context);
    }

    @Override
    public void selectGoto(int selection) {
        navigator.selectInit(selection);
    }
}
