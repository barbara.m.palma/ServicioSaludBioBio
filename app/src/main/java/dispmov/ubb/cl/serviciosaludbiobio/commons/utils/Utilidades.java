package dispmov.ubb.cl.serviciosaludbiobio.commons.utils;

public class Utilidades {

    public String textoMinuscula(String frase) {
        char[] cfr = frase.toCharArray();
        for (int i = 0; i < cfr.length; i++) {
            cfr[i] = Character.toLowerCase(cfr[i]);
        }
        cfr[0] = Character.toUpperCase(cfr[0]);
        for (int i = 0; i < cfr.length; i++) {
            if (cfr[i] == ' ' || cfr[i] == '.' || cfr[i] == ',') {
                cfr[i + 1] = Character.toUpperCase(cfr[i + 1]);
            }
        }
        return String.valueOf(cfr);
    }
}
