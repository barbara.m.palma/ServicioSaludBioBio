package dispmov.ubb.cl.serviciosaludbiobio.ModoGeneral;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;

import dispmov.ubb.cl.serviciosaludbiobio.ModoAdultoMayor.MainAdultoMayorActivity;
import dispmov.ubb.cl.serviciosaludbiobio.Navigation.NavigationActivity;
import dispmov.ubb.cl.serviciosaludbiobio.R;

import com.github.barteksc.pdfviewer.PDFView;

public class CarteraServ extends NavigationActivity {

    PDFView pdfView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cartera_serv);
        setContext(this);

        pdfView = findViewById(R.id.pdfView);
        pdfView.fromAsset("cartera_serv.pdf").load();

    }
    @Override
    public void onBackPressed(){
        Intent iniciar = new Intent(getApplicationContext(), MainPublicoGeneralActivity.class);
        startActivity(iniciar);
        finish();
    }
}
