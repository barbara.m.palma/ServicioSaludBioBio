package dispmov.ubb.cl.serviciosaludbiobio;

import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import android.content.Context;

import dispmov.ubb.cl.serviciosaludbiobio.Entidades.FarmaciaEntidad;
import dispmov.ubb.cl.serviciosaludbiobio.ModoGeneral.MapsActivity;
import dispmov.ubb.cl.serviciosaludbiobio.commons.utils.Utilidades;

public class RecyclerViewAdapterFarmacia extends RecyclerView.Adapter<RecyclerViewAdapterFarmacia.ViewHolder> {

    //Cada RecyclerViewAdapter tiene como atributos un ArrayList de farmacias (las 5 más cercanas) y el contexto
    private ArrayList<FarmaciaEntidad> farmaciasCercanas = new ArrayList<>();
    private Context mContext;

    public RecyclerViewAdapterFarmacia(Context context, ArrayList<FarmaciaEntidad> farmacias) {
        this.farmaciasCercanas = farmacias;
        mContext = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_listitem_farmacia, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        Utilidades u = new Utilidades(); //Se crea un objeto de la Utilidades, para posteriormente procesar texto

        //Se pasan los atributos de todas las farmacias del arreglo a los textView del layout_listitem_farmacia
        //Además se procesan los texto del nombre y dirección de la farmacia para que no salgan todos en mayúscula
        //También se corta el string con la hora de apertura y cierre, para eliminar los segundos
        final String nombre = u.textoMinuscula(farmaciasCercanas.get(position).getNombre_farmacia());
        final String direccion = u.textoMinuscula(farmaciasCercanas.get(position).getDireccion_farmacia() + ", " + farmaciasCercanas.get(position).getNombre_comuna());
        final String horario = farmaciasCercanas.get(position).getHorario_apertura().substring(0, 5) + " - " + farmaciasCercanas.get(position).getHorario_cierre().substring(0, 5);
        holder.nombre.setText(nombre);
        holder.direccion.setText(direccion);
        holder.horario.setText(horario);
        holder.distancia.setText(farmaciasCercanas.get(position).getDistancia() + " Km");

        //Al hacer click en una farmacia de la lista, se creará un intent para abrir un MapsActivity
        //Además, se pasan datos como la latitud, longitud y nombre de la farmacia
        holder.parentLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, MapsActivity.class);
                intent.putExtra("nombre", nombre);
                intent.putExtra("latitud", farmaciasCercanas.get(position).getLatitud());
                intent.putExtra("longitud", farmaciasCercanas.get(position).getLongitud());
                intent.putExtra("direccion", direccion);
                intent.putExtra("horario", horario);
                mContext.startActivity(intent);
            }
        });
    }

    //Entrega la cuenta de elementos del arreglo
    @Override
    public int getItemCount() {
        return farmaciasCercanas.size();
    }


    //Define los elementos del layout_listitem_farmacia
    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView image;
        TextView nombre, direccion, horario, distancia;
        CardView parentLayout;


        public ViewHolder(View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.imagen);
            nombre = itemView.findViewById(R.id.nombre_farmacia);
            direccion = itemView.findViewById(R.id.direccion);
            horario = itemView.findViewById(R.id.horario);
            distancia = itemView.findViewById(R.id.distancia);
            parentLayout = itemView.findViewById(R.id.parent_layout);
        }
    }


}
