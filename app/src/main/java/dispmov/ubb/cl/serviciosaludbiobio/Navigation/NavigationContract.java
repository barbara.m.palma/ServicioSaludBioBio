package dispmov.ubb.cl.serviciosaludbiobio.Navigation;

import android.content.Context;

public interface NavigationContract {
    interface View{

    }

    interface Presenter{
        void setNavigator(Context context);
        void selectGoto(int selection);
    }

    interface Navigator{
        void initHome();
        void selectInit(int selection);
        void initCarteraServ();
        void initConsultarPaciente();
        void initConsultaHora();
        void initDonaSangre();
        void initFarmacia();
        void initVisitas();
        void initContacto();
        void initSaludResponde();
        void initMainAdultoMayorActivity();
    }
}
