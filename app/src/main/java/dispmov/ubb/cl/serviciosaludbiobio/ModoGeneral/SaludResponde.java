package dispmov.ubb.cl.serviciosaludbiobio.ModoGeneral;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import dispmov.ubb.cl.serviciosaludbiobio.Navigation.NavigationActivity;
import dispmov.ubb.cl.serviciosaludbiobio.R;

import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.Button;

public class SaludResponde extends NavigationActivity{

    private Button llamar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_salud_responde);
        setContext(this);

        llamar = findViewById(R.id.llamarSaludResponde);

        llamar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Intent.ACTION_DIAL);
                i.setData(Uri.parse("tel:6003607777"));
                startActivity(i);
            }
        });
    }
    @Override
    public void onBackPressed(){
        Intent iniciar = new Intent(getApplicationContext(), MainPublicoGeneralActivity.class);
        startActivity(iniciar);
        finish();
    }
}
