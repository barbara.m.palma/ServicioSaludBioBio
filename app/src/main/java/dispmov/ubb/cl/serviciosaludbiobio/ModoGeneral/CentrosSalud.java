package dispmov.ubb.cl.serviciosaludbiobio.ModoGeneral;

import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;

import dispmov.ubb.cl.serviciosaludbiobio.Entidades.CentroSalud;
import dispmov.ubb.cl.serviciosaludbiobio.R;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

public class CentrosSalud extends FragmentActivity implements OnMapReadyCallback, GoogleMap.OnMarkerClickListener{

    private GoogleMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_centros_salud);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }
    @Override
    public void onBackPressed(){
        Intent iniciar = new Intent(getApplicationContext(), MainPublicoGeneralActivity.class);
        startActivity(iniciar);
        finish();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        //Luego de que el mapa este listo, se crea un arreglo de centros de salud
        //este arreglo contiene todos los centros de salud entregados
        //se crea un marcador y un objeto LatLng
        CentroSalud[] centros = CentroSalud.CENTROS;
        LatLng centroSalud = new LatLng(centros[0].getLatitud(), centros[0].getLongitud());
        MarkerOptions marker = new MarkerOptions();
        for (int i = 0; i < centros.length; i++) {
            //Se recorre el arreglo de centros de salud
            //Por cada objeto, se modifica el objeto LatLng
            //Se le cambia el titulo y la ubicacion del marcador
            centroSalud = new LatLng(centros[i].getLatitud(), centros[i].getLongitud());
            marker.position(centroSalud)
                    .title(centros[i].getNombre());
            //Dependiendo del tipo de centro que es, se cambiara el color del marcador segun lo siguiente
            //Hospital = blue
            //Sar = rojo
            //sapu = amarillo
            if(centros[i].getTipo()==CentroSalud.TIPO_HOSPITAL){
                marker.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE));
            } else if (centros[i].getTipo()==CentroSalud.TIPO_SAPU){
                marker.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_YELLOW));
            } else if (centros[i].getTipo()==CentroSalud.TIPO_SAR){
                marker.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
            }
            //se añade el marcador al mapa
            mMap.addMarker(marker).showInfoWindow();
        }
        //se anima la camara para que se muestren todos los marcadores
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(centroSalud, 9));
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(marker.getPosition(), 5));
        Log.d("TAG", "click");
        return true;
    }
}
