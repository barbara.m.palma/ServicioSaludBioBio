package dispmov.ubb.cl.serviciosaludbiobio.ModoAdultoMayor;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.github.barteksc.pdfviewer.PDFView;

import dispmov.ubb.cl.serviciosaludbiobio.R;

public class CarteraServAdultMayor extends AppCompatActivity {

    Button retroceder;
    PDFView pdfview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cartera_serv__adult_mayor);

        pdfview = findViewById(R.id.pdfView);
        pdfview.fromAsset("cartera_serv.pdf").load();

        retroceder = findViewById(R.id.retroceder);

        retroceder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent iniciar = new Intent(getApplicationContext(), MainAdultoMayorActivity.class);
                startActivity(iniciar);
                finish();
            }
        });

    }

    @Override
    public void onBackPressed(){
        Intent iniciar = new Intent(getApplicationContext(), MainAdultoMayorActivity.class);
        startActivity(iniciar);
        finish();
    }
}
