package dispmov.ubb.cl.serviciosaludbiobio.ModoGeneral;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;

import dispmov.ubb.cl.serviciosaludbiobio.Navigation.NavigationActivity;
import dispmov.ubb.cl.serviciosaludbiobio.R;

public class Visitas extends NavigationActivity{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_visitas);
        setContext(this);

    }
    @Override
    public void onBackPressed(){
        Intent iniciar = new Intent(getApplicationContext(), MainPublicoGeneralActivity.class);
        startActivity(iniciar);
        finish();
    }
}
