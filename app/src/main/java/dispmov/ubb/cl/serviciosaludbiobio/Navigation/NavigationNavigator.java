package dispmov.ubb.cl.serviciosaludbiobio.Navigation;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import dispmov.ubb.cl.serviciosaludbiobio.ModoGeneral.CarteraServ;
import dispmov.ubb.cl.serviciosaludbiobio.ModoGeneral.CentrosSalud;
import dispmov.ubb.cl.serviciosaludbiobio.ModoGeneral.ConsultaHora;
import dispmov.ubb.cl.serviciosaludbiobio.ModoGeneral.ConsultarPaciente;
import dispmov.ubb.cl.serviciosaludbiobio.ModoGeneral.Contacto;
import dispmov.ubb.cl.serviciosaludbiobio.ModoGeneral.DonaSangre;
import dispmov.ubb.cl.serviciosaludbiobio.ModoGeneral.Farmacia;
import dispmov.ubb.cl.serviciosaludbiobio.ModoAdultoMayor.MainAdultoMayorActivity;
import dispmov.ubb.cl.serviciosaludbiobio.ModoGeneral.MainPublicoGeneralActivity;
import dispmov.ubb.cl.serviciosaludbiobio.ModoGeneral.SaludResponde;
import dispmov.ubb.cl.serviciosaludbiobio.ModoGeneral.Visitas;
import dispmov.ubb.cl.serviciosaludbiobio.R;

import static android.content.Context.MODE_PRIVATE;
import static dispmov.ubb.cl.serviciosaludbiobio.commons.utils.Constans.Modo.MODO_ADULTO_M;

public class NavigationNavigator implements NavigationContract.Navigator {
    private Activity context;
    private Intent iniciar;

    private SharedPreferences sharedPre;
    private SharedPreferences.Editor editorSP;

    public NavigationNavigator(Context context) {
        this.context = (Activity) context;
        sharedPre = context.getSharedPreferences(context.getString(R.string.sharedPreID), MODE_PRIVATE);
        editorSP = sharedPre.edit();
        editorSP.commit();
    }

    @Override
    public void selectInit(int selection) {
        switch (selection) {
            case R.id.home:
                initHome();
                break;
            case R.id.nav_cartera:
                initCarteraServ();
                break;

            case R.id.nav_estado:
                initConsultarPaciente();
                break;

            case R.id.nav_hora:
                initConsultaHora();
                break;

            case R.id.nav_sangre:
                initDonaSangre();
                break;

            case R.id.nav_farmacia:
                initFarmacia();
                break;

            case R.id.nav_visitas:
                initVisitas();
                break;

            case R.id.nav_contacto:
                initContacto();
                break;

            case R.id.nav_fono:
                initSaludResponde();
                break;

            case R.id.nav_modo:
                initMainAdultoMayorActivity();
                break;

            case R.id.nav_centros_salud:
                initCentrosDeSalud();
                break;
        }
    }

    private void initCentrosDeSalud() {
        iniciar = new Intent(context, CentrosSalud.class);
        context.startActivity(iniciar);
        context.finish();
    }

    @Override
    public void initHome() {
        iniciar = new Intent(context, MainPublicoGeneralActivity.class);
        context.startActivity(iniciar);
        context.finish();
    }

    @Override
    public void initCarteraServ() {
        iniciar = new Intent(context, CarteraServ.class);
        context.startActivity(iniciar);
        context.finish();
    }

    @Override
    public void initConsultarPaciente() {
        iniciar = new Intent(context, ConsultarPaciente.class);
        context.startActivity(iniciar);
        context.finish();
    }

    @Override
    public void initConsultaHora() {
        iniciar = new Intent(context, ConsultaHora.class);
        context.startActivity(iniciar);
        context.finish();
    }

    @Override
    public void initDonaSangre() {
        iniciar = new Intent(context, DonaSangre.class);
        context.startActivity(iniciar);
        context.finish();
    }

    @Override
    public void initFarmacia() {
        iniciar = new Intent(context, Farmacia.class);
        context.startActivity(iniciar);
        context.finish();
    }

    @Override
    public void initVisitas() {
        iniciar = new Intent(context, Visitas.class);
        context.startActivity(iniciar);
        context.finish();
    }

    @Override
    public void initContacto() {
        iniciar = new Intent(context, Contacto.class);
        context.startActivity(iniciar);
        context.finish();
    }

    @Override
    public void initSaludResponde() {
        iniciar = new Intent(context, SaludResponde.class);
        context.startActivity(iniciar);
        context.finish();
    }

    @Override
    public void initMainAdultoMayorActivity() {
        editorSP.putInt("MODO", MODO_ADULTO_M);
        editorSP.commit();
        iniciar = new Intent(context, MainAdultoMayorActivity.class);
        context.startActivity(iniciar);
        context.finish();
    }
}
