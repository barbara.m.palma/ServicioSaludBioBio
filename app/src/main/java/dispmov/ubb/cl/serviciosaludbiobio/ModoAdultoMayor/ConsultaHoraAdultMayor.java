package dispmov.ubb.cl.serviciosaludbiobio.ModoAdultoMayor;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.util.Xml;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import dispmov.ubb.cl.serviciosaludbiobio.Entidades.PacienteEntidad;
import dispmov.ubb.cl.serviciosaludbiobio.ModoGeneral.ConsultaHora;
import dispmov.ubb.cl.serviciosaludbiobio.R;
import dispmov.ubb.cl.serviciosaludbiobio.RecyclerViewConsultaHora;
import dispmov.ubb.cl.serviciosaludbiobio.commons.utils.Utilidades;

public class ConsultaHoraAdultMayor extends AppCompatActivity {

    private Button retroceder;
    private static final String ns = null;

    private static final String ETIQUETA_CODIGO = "codigo";
    private static final String ETIQUETA_DESCRIPCION = "descripcion";
    private static final String ETIQUETA_PACIENTE = "paciente";
    private static final String ETIQUETA_FECHA_ASIGNADA = "fecha_asignada";
    private static final String ETIQUETA_PROFESIONAL = "profesional";
    private static final String ETIQUETA_POLICLINICO = "policlinico";
    private static final String ETIQUETA_UBICACION = "ubicacion";
    private static final String ETIQUETA_TIPO_HORA = "tipo_hora";
    private static final String ETIQUETA_ITEM = "item";
    private static final String ETIQUETA_RETURN = "return";
    //Crea variables que permitirán interactuar con la información del layout
    private EditText textoRut, textodv;
    private TextView nombrePaciente, nombre;
    private Button botonBuscar;
    private int parametroRut;
    private String parametrodv, cadena;
    private List<PacienteEntidad> lista;
    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_consulta_hora__adult_mayor);

        textoRut = findViewById(R.id.rut);
        textodv = findViewById(R.id.digitoVerificador);
        nombrePaciente = findViewById(R.id.nombrePaciente);
        recyclerView = findViewById(R.id.Lista_paciente); //se crea un objeto de tipo Recyclerview y se enlaza con el layout correspondiente
        botonBuscar = findViewById(R.id.botonBuscar);
        nombre = findViewById(R.id.nombre);

        botonBuscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Recibe desde teclado el rut ingresado
                cadena = textoRut.getText().toString();
                //se guarda el digito verificador ingresado desde teclado
                parametrodv = textodv.getText().toString();
                //Condición que verífica que la cadena ingresada del rut no esté vacía
                if(TextUtils.isEmpty(cadena) || TextUtils.isEmpty(parametrodv)) {
                    nombrePaciente.setText("");
                    recyclerView.setAdapter(null);
                    recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
                    Toast.makeText(ConsultaHoraAdultMayor.this, "Error en la llamada, parámetros insuficientes", Toast.LENGTH_SHORT).show();
                    return;
                }

                //se guarda el rut ingresado de la varialbe cadena para convertirlo en tipo entero
                parametroRut = Integer.parseInt(cadena);
                //llamada al metodo webservice
                callWebService();
            }
        });
        retroceder = findViewById(R.id.retroceder);
        retroceder.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent iniciar = new Intent(getApplicationContext(), MainAdultoMayorActivity.class);
                startActivity(iniciar);
                finish();
            }
        });
    }
    @Override
    public void onBackPressed(){
        Intent iniciar = new Intent(getApplicationContext(), MainAdultoMayorActivity.class);
        startActivity(iniciar);
        finish();
    }

    private void callWebService(){
        String WS_URL = "http://10.8.117.115/ws/SAC/Servicios_Usuarios/server.php";
        final String requestBody = getXMLBody(parametroRut, parametrodv.toUpperCase());
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        StringRequest request = new StringRequest(
                Request.Method.POST,
                WS_URL,
                new Response.Listener<String>(){
                    @Override
                    public void onResponse(String response) {
                        Log.d("LOG WS", response);
                        XmlPullParser parser = Xml.newPullParser();
                        InputStream stream = new ByteArrayInputStream(response.getBytes(StandardCharsets.UTF_8));
                        try {
                            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, true);
                            parser.setInput(stream, null);
                            parser.nextTag();

                            while(!parser.getName().equals(ETIQUETA_RETURN)){
                                Log.d("LOG", "name: " + parser.getName());
                                parser.nextTag();
                            }
                            lista = leerHoras(parser);
                            if(lista.size() == 1){
                                if(!lista.get(0).getCodigo().equals("1")) {
                                    Log.e("ERROR", "DESCRIPCION: " + lista.get(0).getDescripcion_codigo());
                                    nombrePaciente.setText("");
                                    recyclerView.setAdapter(null);
                                    recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
                                    Toast.makeText(ConsultaHoraAdultMayor.this, lista.get(0).getDescripcion_codigo(), Toast.LENGTH_SHORT).show();
                                    return;
                                }
                            }
                            //se crea un objeto de tipo utilidades para modificar el texto recibidos del servicio del paciente
                            Utilidades u = new Utilidades();
                            RecyclerViewConsultaHora adapter = new RecyclerViewConsultaHora(getApplicationContext(), (ArrayList<PacienteEntidad>) lista);
                            recyclerView.setAdapter(adapter);
                            recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
                            nombre.setText("Nombre Paciente:");
                            //se modifica el nombre del paciente en la interfaz y se modifica el texto a minusculas
                            nombrePaciente.setText(u.textoMinuscula(lista.get(0).getPaciente()));

                        } catch (XmlPullParserException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("LOG WS", error.toString());
            }
        }
        ){
            @Override
            public String getBodyContentType() {
                return "text/xml;charset=UTF-8";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("SOAPAction", "urn:WS#HorasAsignadasUsuarios");
                //params.put("Content-Type","text/xml;charset=UTF-8");
                return params;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                Log.d("LOG WSSS", "body: " + requestBody);
                try {
                    return requestBody == null ? null : requestBody.getBytes("UTF-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }
        };
        request.setShouldCache(false);
        requestQueue.add(request);
    }

    private List<PacienteEntidad> leerHoras(XmlPullParser parser)
            throws XmlPullParserException, IOException {
        List<PacienteEntidad> listaPacientess = new ArrayList<>();
        parser.require(XmlPullParser.START_TAG, ns, ETIQUETA_RETURN);
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String nombreEtiqueta = parser.getName();
            if (nombreEtiqueta.equals(ETIQUETA_ITEM)) {
                PacienteEntidad x = leerHora(parser);
                //Se agrega a la lista lo que retornó la función leer hora
                listaPacientess.add(x);
            } else {
                saltarEtiqueta(parser);
            }
        }
        return listaPacientess;
    }

    private PacienteEntidad leerHora(XmlPullParser parser) throws XmlPullParserException, IOException {
        parser.require(XmlPullParser.START_TAG, ns, ETIQUETA_ITEM);
        String codigo = null;
        String descripcion_codigo = null;
        String paciente = null;
        String fecha_asignada = null;
        String profesional = null;
        String policlinico = null;
        String ubicacion = null;
        String tipo_hora = null;

        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String name = parser.getName();
            switch (name) {
                case ETIQUETA_CODIGO:
                    codigo = leerCodigo(parser);
                    break;
                case ETIQUETA_DESCRIPCION:
                    descripcion_codigo = leerDescripcion(parser);
                    break;
                case ETIQUETA_PACIENTE:
                    paciente = leerPaciente(parser);
                    break;
                case ETIQUETA_FECHA_ASIGNADA:
                    fecha_asignada = leerFecha(parser);
                    break;
                case ETIQUETA_PROFESIONAL:
                    profesional = leerProfesional(parser);
                    break;
                case ETIQUETA_POLICLINICO:
                    policlinico = leerPoliclinico(parser);
                    break;
                case ETIQUETA_UBICACION:
                    ubicacion = leerUbicacion(parser);
                    break;
                case ETIQUETA_TIPO_HORA:
                    tipo_hora = leerTipoHora(parser);
                    break;
                default:
                    saltarEtiqueta(parser);
                    break;
            }
        }
        return new PacienteEntidad(codigo,descripcion_codigo,paciente,fecha_asignada,profesional,policlinico,ubicacion,tipo_hora);
    }

    private String leerCodigo(XmlPullParser parser) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, ns, ETIQUETA_CODIGO);
        String nombre = obtenerTexto(parser);
        parser.require(XmlPullParser.END_TAG, ns, ETIQUETA_CODIGO);
        return nombre;
    }

    private String leerDescripcion(XmlPullParser parser) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, ns, ETIQUETA_DESCRIPCION);
        String nombre = obtenerTexto(parser);
        parser.require(XmlPullParser.END_TAG, ns, ETIQUETA_DESCRIPCION);
        return nombre;
    }

    private String leerPaciente(XmlPullParser parser) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, ns, ETIQUETA_PACIENTE);
        String nombre = obtenerTexto(parser);
        parser.require(XmlPullParser.END_TAG, ns, ETIQUETA_PACIENTE);
        return nombre;
    }

    private String leerFecha(XmlPullParser parser) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, ns, ETIQUETA_FECHA_ASIGNADA);
        String nombre = obtenerTexto(parser);
        parser.require(XmlPullParser.END_TAG, ns, ETIQUETA_FECHA_ASIGNADA);
        return nombre;
    }

    private String leerProfesional(XmlPullParser parser) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, ns, ETIQUETA_PROFESIONAL);
        String nombre = obtenerTexto(parser);
        parser.require(XmlPullParser.END_TAG, ns, ETIQUETA_PROFESIONAL);
        return nombre;
    }

    private String leerPoliclinico(XmlPullParser parser) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, ns, ETIQUETA_POLICLINICO);
        String nombre = obtenerTexto(parser);
        parser.require(XmlPullParser.END_TAG, ns, ETIQUETA_POLICLINICO);
        return nombre;
    }

    private String leerUbicacion(XmlPullParser parser) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, ns, ETIQUETA_UBICACION);
        String nombre = obtenerTexto(parser);
        parser.require(XmlPullParser.END_TAG, ns, ETIQUETA_UBICACION);
        return nombre;
    }

    private String leerTipoHora(XmlPullParser parser) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, ns, ETIQUETA_TIPO_HORA);
        String nombre = obtenerTexto(parser);
        parser.require(XmlPullParser.END_TAG, ns, ETIQUETA_TIPO_HORA);
        return nombre;
    }

    private String obtenerTexto(XmlPullParser parser) throws IOException, XmlPullParserException {
        String resultado = "";
        if (parser.next() == XmlPullParser.TEXT) {
            resultado = parser.getText();
            parser.nextTag();
        }
        return resultado;
    }

    public String   getXMLBody(int rut, String dv) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("<soapenv:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:urn=\"urn:WS\">");
        stringBuilder.append("<soapenv:Header/>");
        stringBuilder.append("<soapenv:Body>");
        stringBuilder.append("<urn:HorasAsignadasUsuarios soapenv:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">");
        stringBuilder.append("<proArray xsi:type=\"urn:ArrayReq\">");
        stringBuilder.append("<rut_paciente xsi:type=\"xsd:int\">" + rut +"</rut_paciente>");
        stringBuilder.append("<dv_paciente xsi:type=\"xsd:string\">" + dv +"</dv_paciente>");
        stringBuilder.append("<token xsi:type=\"xsd:string\">qmVq2x7Yxm</token>");
        stringBuilder.append("</proArray>");
        stringBuilder.append("</urn:HorasAsignadasUsuarios>");
        stringBuilder.append("</soapenv:Body>");
        stringBuilder.append("</soapenv:Envelope>");
        String result = stringBuilder.toString();
        Log.d("LOG", result);
        return result;
    }

    private void saltarEtiqueta(XmlPullParser parser) throws XmlPullParserException, IOException {
        if (parser.getEventType() != XmlPullParser.START_TAG) {
            throw new IllegalStateException();
        }
        int depth = 1;
        while (depth != 0) {
            switch (parser.next()) {
                case XmlPullParser.END_TAG:
                    depth--;
                    break;
                case XmlPullParser.START_TAG:
                    depth++;
                    break;
            }
        }
    }

}
