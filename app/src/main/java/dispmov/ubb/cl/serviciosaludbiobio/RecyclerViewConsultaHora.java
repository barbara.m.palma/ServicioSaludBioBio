package dispmov.ubb.cl.serviciosaludbiobio;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import dispmov.ubb.cl.serviciosaludbiobio.Entidades.PacienteEntidad;
import dispmov.ubb.cl.serviciosaludbiobio.commons.utils.Utilidades;


public class RecyclerViewConsultaHora extends RecyclerView.Adapter<RecyclerViewConsultaHora.ViewHolder>{
    //Cada RecyclerViewAdapter tiene como atributos un ArrayList del paciente
    private ArrayList<PacienteEntidad> nombres = new ArrayList<>();
    private Context mContext;

    public RecyclerViewConsultaHora(Context context, ArrayList<PacienteEntidad> nombres ) {
        this.nombres = nombres;
        mContext = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_listitem_consultahora, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        Utilidades utilidades = new Utilidades(); //Se crea un objeto de la Utilidades, para posteriormente procesar texto
        //Se pasan los atributos del paciente del arreglo a los textView del layout_listitem_consultahora
        //Además se procesan los textos correspondientes del profesional, modulo y policlinico
        holder.datoProfesional.setText(utilidades.textoMinuscula(nombres.get(position).getProfesional()));
        holder.datoPoliclinico.setText(utilidades.textoMinuscula(nombres.get(position).getPoliclinico()));
        holder.datoFecha.setText(nombres.get(position).getFecha_asignada());
        holder.datoUbicacion.setText(utilidades.textoMinuscula(nombres.get(position).getUbicacion()));
        holder.parentLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //Toast.makeText(mContext, nombres.get(position).getPaciente(), Toast.LENGTH_SHORT).show();

              /*  Intent intent = new Intent(mContext, GalleryActivity.class);
                intent.putExtra("image_url", mImages.get(position));
                intent.putExtra("image_name", mImageNames.get(position));
                mContext.startActivity(intent);*/
            }
        });
    }

    @Override
    public int getItemCount() {
        return nombres.size();
    }

    //Define los elementos del layout_listitem_consultahora
    public class ViewHolder extends RecyclerView.ViewHolder{

        TextView datoProfesional, datoPoliclinico, datoFecha, datoUbicacion;
        CardView parentLayout;

        public ViewHolder(View itemView) {
            super(itemView);
            parentLayout = itemView.findViewById(R.id.parent_layout);
            datoProfesional = itemView.findViewById(R.id.datoProfesional);
            datoPoliclinico = itemView.findViewById(R.id.datoPoliclinico);
            datoFecha = itemView.findViewById(R.id.fechaAsignada);
            datoUbicacion= itemView.findViewById(R.id.datoUbicacion);
        }
    }
}
