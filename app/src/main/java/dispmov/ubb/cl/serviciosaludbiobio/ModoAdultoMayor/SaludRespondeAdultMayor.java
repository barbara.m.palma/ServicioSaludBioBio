package dispmov.ubb.cl.serviciosaludbiobio.ModoAdultoMayor;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import dispmov.ubb.cl.serviciosaludbiobio.R;

public class SaludRespondeAdultMayor extends AppCompatActivity {

    private Button retroceder, llamar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_salud_responde__adult_mayor);

        retroceder = findViewById(R.id.retroceder);
        llamar = findViewById(R.id.llamarSaludResponde);

        llamar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Intent.ACTION_DIAL);
                i.setData(Uri.parse("tel:6003607777"));
                startActivity(i);
            }
        });

        retroceder.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent iniciar = new Intent(getApplicationContext(), MainAdultoMayorActivity.class);
                startActivity(iniciar);
                finish();
            }
        });

    }
    @Override
    public void onBackPressed(){
        Intent iniciar = new Intent(getApplicationContext(), MainAdultoMayorActivity.class);
        startActivity(iniciar);
        finish();
    }
}
