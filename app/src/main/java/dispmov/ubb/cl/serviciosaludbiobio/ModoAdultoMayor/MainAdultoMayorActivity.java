package dispmov.ubb.cl.serviciosaludbiobio.ModoAdultoMayor;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import dispmov.ubb.cl.serviciosaludbiobio.DialogCustom;
import dispmov.ubb.cl.serviciosaludbiobio.ModoGeneral.CentrosSalud;
import dispmov.ubb.cl.serviciosaludbiobio.ModoGeneral.MainPublicoGeneralActivity;
import dispmov.ubb.cl.serviciosaludbiobio.R;

public class MainAdultoMayorActivity extends AppCompatActivity implements DialogCustom.ListenerButton {

    private MainAdultoMayorActivity _this = this;
    private int MODO_GENERAL = 1;
    private static final int ID_DIALOG_MODO_GRAFICO = 0;
    private static final int ID_DIALOG_SALIR = 1;

    private SharedPreferences sharedPre;
    private SharedPreferences.Editor editorSP;
    Button siguientePagina, cambiarModo, centrosDeSalud;
    ImageView carteleraServicios, consultaPaciente, consultaHoraMedica, horarioVisita, farmaciaTurno, donarSangre;

    private DialogCustom dlog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_adulto_mayor);

        sharedPre = getSharedPreferences(getString(R.string.sharedPreID), MODE_PRIVATE);
        editorSP = sharedPre.edit();
        editorSP.commit();

        donarSangre = findViewById(R.id.donacionSangre);
        carteleraServicios = findViewById(R.id.carteraDeServicio);
        consultaHoraMedica = findViewById(R.id.consultaHora);
        consultaPaciente = findViewById(R.id.consultarEstado);
        farmaciaTurno = findViewById(R.id.farmacia);
        horarioVisita = findViewById(R.id.horarioVisita);
        cambiarModo = findViewById(R.id.cambiarModo);
        centrosDeSalud = findViewById(R.id.ubicacionCentroSalud);
        siguientePagina = findViewById(R.id.saludResponde);

        siguientePagina.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent iniciar = new Intent(_this, SaludRespondeAdultMayor.class);
                startActivity(iniciar);
                finish();
            }
        });

        centrosDeSalud.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent iniciar = new Intent(_this, CentrosSaludAdultoMayor.class);
                startActivity(iniciar);
                finish();
            }
        });

        cambiarModo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dlog = new DialogCustom();
                dlog.setIdDialog(ID_DIALOG_MODO_GRAFICO);
                dlog.setTxt_msg("¿Está seguro de que desea cambiar el estilo la aplicación?");
                dlog.setTxt_title("Servicio de Salud Bio-Bio");
                dlog.setTxt_btn_neg("No, continuar con Vista Simple");
                dlog.setTxt_btn_pos("Si, cambiar a Vista Avanzada");
                dlog.show(getFragmentManager(),"My Dialog Custom");
            }
        });

        donarSangre.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent iniciar = new Intent(_this, DonaSangreAdultMayor.class);
                startActivity(iniciar);
                finish();
            }
        });

        carteleraServicios.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent iniciar = new Intent(_this, CarteraServAdultMayor.class);
                startActivity(iniciar);
                finish();
            }
        });

        horarioVisita.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent iniciar = new Intent(_this, VisitasAdultMayor.class);
                startActivity(iniciar);
                finish();
            }
        });

        consultaHoraMedica.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent iniciar = new Intent(_this, ConsultaHoraAdultMayor.class);
                startActivity(iniciar);
                finish();
            }
        });

        consultaPaciente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent iniciar = new Intent(_this, ConsultarPacienteAdultMayor.class);
                startActivity(iniciar);
                finish();
            }
        });

        farmaciaTurno.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent iniciar = new Intent(_this, FarmaciaAdultMayor.class);
                startActivity(iniciar);
                finish();
            }
        });

    }

    @Override
    public void onClickNegButton(int idDialog) {
        dlog.dismiss();
    }

    @Override
    public void onClickPosButton(int idDialog) {
        switch (idDialog) {
            case ID_DIALOG_MODO_GRAFICO:
                editorSP.putInt("MODO",MODO_GENERAL);
                editorSP.commit();
                Intent iniciar = new Intent(_this, MainPublicoGeneralActivity.class);
                startActivity(iniciar);
                finish();
                break;
            case ID_DIALOG_SALIR:
                finish();
                break;
        }
    }

    @Override
    public void onBackPressed(){
        dlog = new DialogCustom();
        dlog.setIdDialog(ID_DIALOG_SALIR);
        dlog.setTxt_msg("¿Está seguro de que desea salir de la aplicacion?");
        dlog.setTxt_title("Servicio de Salud Bio-Bio");
        dlog.setTxt_btn_neg("Cancelar");
        dlog.setTxt_btn_pos("Salir");
        dlog.show(getFragmentManager(),"My Dialog Custom");
    }
    
}
