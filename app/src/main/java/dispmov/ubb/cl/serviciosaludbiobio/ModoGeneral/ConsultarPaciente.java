package dispmov.ubb.cl.serviciosaludbiobio.ModoGeneral;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.util.Xml;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import dispmov.ubb.cl.serviciosaludbiobio.Navigation.NavigationActivity;
import dispmov.ubb.cl.serviciosaludbiobio.R;

public class ConsultarPaciente extends NavigationActivity{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_consultar_paciente);
        setContext(this);

    }

    @Override
    public void onBackPressed(){
        Intent iniciar = new Intent(getApplicationContext(), MainPublicoGeneralActivity.class);
        startActivity(iniciar);
        finish();
    }

}
