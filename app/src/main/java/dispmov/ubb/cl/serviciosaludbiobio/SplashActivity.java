package dispmov.ubb.cl.serviciosaludbiobio;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import dispmov.ubb.cl.serviciosaludbiobio.ModoAdultoMayor.MainAdultoMayorActivity;
import dispmov.ubb.cl.serviciosaludbiobio.ModoGeneral.MainPublicoGeneralActivity;

public class SplashActivity extends AppCompatActivity implements DialogCustom.ListenerButton{

    private SplashActivity _this = this;

    private SharedPreferences sharedPre;
    private SharedPreferences.Editor editorSP;

    private int NO_PREFERENCES = 0;
    private int MODO_GENERAL = 1;
    private int MODO_ADULTO_M= 2;

    private static final int ID_DIALOG_MODO_GRAFICO = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);


        sharedPre = getSharedPreferences(getString(R.string.sharedPreID), MODE_PRIVATE);
        editorSP = sharedPre.edit();

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent iniciar = null;
                if(sharedPre.getInt("MODO", NO_PREFERENCES) == NO_PREFERENCES){
                    DialogCustom dlog = new DialogCustom();
                    dlog.setIdDialog(ID_DIALOG_MODO_GRAFICO);
                    dlog.setTxt_msg("Elija un modo gráfico");
                    dlog.setTxt_title("Servicio de Salud Bio-Bio");
                    dlog.setTxt_btn_neg("Vista Simple");
                    dlog.setTxt_btn_pos("Vista Avanzada ");
                    dlog.show(getFragmentManager(),"My Dialog Custom");
                }else{
                    if(sharedPre.getInt("MODO", NO_PREFERENCES) == MODO_GENERAL){
                        iniciar = new Intent(_this, MainPublicoGeneralActivity.class);
                    }else{
                        iniciar = new Intent(_this, MainAdultoMayorActivity.class);
                    }
                    startActivity(iniciar);
                    finish();
                }
            }
        }, 2000);//Milisegundos

    }



    @Override
    public void onClickNegButton(int idDialog) {
        switch (idDialog){
            case ID_DIALOG_MODO_GRAFICO:
                editorSP.putInt("MODO",MODO_ADULTO_M);
                editorSP.commit();
                Intent iniciar = new Intent(_this, MainAdultoMayorActivity.class);
                startActivity(iniciar);
                finish();
                break;
        }
    }

    @Override
    public void onClickPosButton(int idDialog) {
        switch (idDialog) {
            case ID_DIALOG_MODO_GRAFICO:
                editorSP.putInt("MODO",MODO_GENERAL);
                editorSP.commit();
                Intent iniciar = new Intent(_this, MainPublicoGeneralActivity.class);
                startActivity(iniciar);
                finish();
                break;
        }
    }
}
