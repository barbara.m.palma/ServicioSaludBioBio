package dispmov.ubb.cl.serviciosaludbiobio.ModoAdultoMayor;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.LocationSource;
import com.google.android.gms.maps.OnMapReadyCallback;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import dispmov.ubb.cl.serviciosaludbiobio.Entidades.FarmaciaEntidad;
import dispmov.ubb.cl.serviciosaludbiobio.ModoGeneral.Farmacia;
import dispmov.ubb.cl.serviciosaludbiobio.PermissionUtils;
import dispmov.ubb.cl.serviciosaludbiobio.R;
import dispmov.ubb.cl.serviciosaludbiobio.RecyclerViewAdapterFarmacia;
import dispmov.ubb.cl.serviciosaludbiobio.RecyclerViewAdapterFarmaciaAdultoMayor;

public class FarmaciaAdultMayor extends AppCompatActivity implements OnMapReadyCallback, LocationSource{

    private Button retroceder;
    private ArrayList<FarmaciaEntidad> farmaciasDeTurno;
    private ArrayList<FarmaciaEntidad> cincoCercanas;

    private RecyclerView recyclerView;
    private RecyclerViewAdapterFarmaciaAdultoMayor adapter;
    private LinearLayoutManager linearLayoutManager;
    private Location mCurrentLocation;
    private LocationManager mLocationManager;
    private static long UPDATE_INTERVAL_IN_MILLISECONDS;
    private static final int LOCATION_PERMISSION_REQUEST_CODE = 1;
    private boolean mPermissionDenied = false;
    private GoogleMap mMap;
    private LocationSource.OnLocationChangedListener listener;
    private DividerItemDecoration dividerItemDecoration;
    private TextView distancia;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_farmacia__adult_mayor);

        retroceder = findViewById(R.id.retroceder);
        distancia = findViewById(R.id.distancia);

        retroceder.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent iniciar = new Intent(getApplicationContext(), MainAdultoMayorActivity.class);
                startActivity(iniciar);
                finish();
            }
        });

        //Crea los arreglos que contienen todas las farmacias y las cinco mas cercanas
        farmaciasDeTurno = new ArrayList<>();
        cincoCercanas =  new ArrayList<>();


        /** mLocationManager gestiona las peticiones de posicion **/
        mLocationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        /** Verificamos si el usuario tiene encendido el GPS, si no,
         * lo enviamos al menú para que lo encienda **/
        boolean enabledGPS = mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        if (!enabledGPS) {
            Toast.makeText(this, "No hay señal de GPS", Toast.LENGTH_LONG).show();
            Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            startActivity(intent);
        }

        /** Establecemos el intervalo de actualización de la posicion **/
        UPDATE_INTERVAL_IN_MILLISECONDS = 5000;

        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) !=
                PackageManager.PERMISSION_GRANTED) {
            PermissionUtils.requestPermission(this, LOCATION_PERMISSION_REQUEST_CODE,
                    Manifest.permission.ACCESS_FINE_LOCATION, true);
        }else{
            enableMyLocation();
        }
    }
    @Override
    public void onBackPressed(){
        Intent iniciar = new Intent(getApplicationContext(), MainAdultoMayorActivity.class);
        startActivity(iniciar);
        finish();
    }

    private void serviceFarmacias(){
        Log.d("LOG WS", "entre");
        String WS_URL = "http://farmanet.minsal.cl/index.php/ws/getLocalesTurnos";
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        StringRequest request = new StringRequest(
                Request.Method.GET,
                WS_URL,
                new Response.Listener<String>(){
                    @Override
                    public void onResponse(String response) {
                        try {
                            //analiza la respuesta Json del servicio web del ministerio de salud
                            JSONArray responseJson = new JSONArray(response);
                            for(int i = 0; i < responseJson.length(); i++){
                                JSONObject o = responseJson.getJSONObject(i);
                                //Crea un nuevo objeto FarmaciaEntidad y le asigna los valores obtenidos desde el json
                                FarmaciaEntidad far = new FarmaciaEntidad();

                                far.setFecha(o.getString("fecha"));
                                far.setLocal_id(o.getString("local_id"));
                                far.setRegion_id(o.getString("fk_region"));
                                far.setComuna_id(o.getString("fk_comuna"));
                                far.setLocalidad_id(o.getString("fk_localidad"));
                                far.setNombre_farmacia(o.getString("local_nombre"));
                                far.setNombre_comuna(o.getString("comuna_nombre"));
                                far.setDireccion_farmacia(o.getString("local_direccion"));
                                far.setHorario_apertura(o.getString("funcionamiento_hora_apertura"));
                                far.setHorario_cierre(o.getString("funcionamiento_hora_cierre"));
                                far.setTelefono(o.getString("local_telefono"));
                                String lat = o.getString("local_lat");
                                String lng = o.getString("local_lng");
                                far.setDistancia(Integer.MAX_VALUE);

                                try{
                                    far.setLatitud(Double.parseDouble(lat));
                                    far.setLongitud(Double.parseDouble(lng));
                                }catch (NumberFormatException e){
                                    far.setLatitud(0);
                                    far.setLongitud(0);
                                }

                                //Añade la nueva farmacia al arreglo
                                farmaciasDeTurno.add(far);

                            }

                            Log.d("LOG", "cantidad: " + farmaciasDeTurno.size());
                            //en caso de no ser posible obtener la ubicación la aplicación mostrará un toast con el mensaje correspondiente
                            if(mCurrentLocation==null){
                                generateToast("Error, no es posible obtener tu ubicación.");
                                return;
                            }
                            //Llamada al método que calcula la distancia entre todas las farmacias y la ubicación del usuario
                            farmaciasDeTurno = calculaDistancias(farmaciasDeTurno);
                            //Llamada al método que ordena las farmacias de turno según su distancia
                            Collections.sort(farmaciasDeTurno);
                            //añade las 5 primeras posiciones del arreglo de farmacias al arreglo de las farmacias más cercanas
                            cincoCercanas.add(farmaciasDeTurno.get(0));
                            cincoCercanas.add(farmaciasDeTurno.get(1));
                            cincoCercanas.add(farmaciasDeTurno.get(2));
                            cincoCercanas.add(farmaciasDeTurno.get(3));
                            cincoCercanas.add(farmaciasDeTurno.get(4));
                            //Crea un linearLayoutManager, RecyclerView, dividerItemDecoration y RecyclerViewAdapter
                            linearLayoutManager = new LinearLayoutManager(getApplicationContext());
                            dividerItemDecoration = new DividerItemDecoration(getApplicationContext(), linearLayoutManager.getOrientation());
                            distancia.setText("Distancia");
                            recyclerView = findViewById(R.id.listaFarmacias);
                            adapter = new RecyclerViewAdapterFarmaciaAdultoMayor(getApplicationContext(), cincoCercanas);
                            //Le indica al recyclerView cual es su adaptador, layoutManager y divideerItemDecoration
                            recyclerView.setAdapter(adapter);
                            recyclerView.setLayoutManager(linearLayoutManager);
                            recyclerView.addItemDecoration(dividerItemDecoration);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("LOG WS", error.toString());
                generateToast("Error, no se pueden consultar las farmacias de turno en este momento.");
            }
        }
        );
        requestQueue.add(request);
        Log.d("LOG WS", String.valueOf(farmaciasDeTurno.size()));
    }


    //Método que calcula la distancia entre todas las farmacias de turno y la ubicación del usuario
    private ArrayList<FarmaciaEntidad> calculaDistancias(ArrayList<FarmaciaEntidad> farmaciasDeTurno) {
        //Se recorre el arraylist y por cada farmacia se calcula la distancia y se guarda en el atributo "Distancia"
        for (int i = 0; i < farmaciasDeTurno.size()-1; i++) {
            farmaciasDeTurno.get(i).setDistancia(distancia(farmaciasDeTurno.get(i).getLatitud(),
                    farmaciasDeTurno.get(i).getLongitud(), mCurrentLocation.getLatitude(),
                    mCurrentLocation.getLongitude()));
        }
        return farmaciasDeTurno;
    }

    //Método que calcula la distancia entre 2 latitudes y longitudes
    private int distancia(double lat1, double lng1, double lat2, double lng2){
        double R = 6378.137;
        double dLat  = rad( lat2 - lat1 );
        double dLong = rad( lng2 - lng1 );

        double a = Math.sin(dLat/2) * Math.sin(dLat/2) + Math.cos(rad(lat1)) * Math.cos(rad(lat2)) * Math.sin(dLong/2) * Math.sin(dLong/2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        double d = R * c;

        return (int) d;
    }

    private double rad(double data){
        return data * Math.PI/180;
    }


    private void generateToast(String msg){
        Toast.makeText(getApplicationContext(),msg, Toast.LENGTH_SHORT).show();
    }

    /** gestionamos la respuesta de la petición de permisos **/
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode != LOCATION_PERMISSION_REQUEST_CODE) {
            return;
        }

        if (PermissionUtils.isPermissionGranted(permissions, grantResults,
                Manifest.permission.ACCESS_FINE_LOCATION)) {
            /** si el permiso fue aceptado, iniciamos el proceso de captura de posiciones **/
            enableMyLocation();
        } else {
            mPermissionDenied = true;
        }
    }

    //Activa la ubicacion
    private void enableMyLocation() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            /** Si el permiso no fue concedido o no ha sido solicitado, se solicita **/
            PermissionUtils.requestPermission(this, LOCATION_PERMISSION_REQUEST_CODE,
                    Manifest.permission.ACCESS_FINE_LOCATION, true);
        } else if (mMap != null) {
            /** Cuando ya tenemos los permisos
             * le decimos al mapa que capture la posicion y
             * modificamos de donde se obtiene la posición,
             * con el objetivo de contralar como y cuando se actualiza **/
            mMap.setMyLocationEnabled(true);
            mMap.setLocationSource(this);

            /** Se le dice de donde se captura la posicion, en este caso el GPS(puede ser desde internet),
             * el intervalo de actualizacion,
             * la distancia minima que debe modificar la posicion para ser requerida una actualizacion
             * y el evento que capura la posicion**/
            mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, UPDATE_INTERVAL_IN_MILLISECONDS, 10, new android.location.LocationListener() {
                @Override
                public void onLocationChanged(Location location) {
                    /** cuando se captura una nueva posicion, se le entrega al evento que fue seteado en el mapa
                     * para que sea consiente se su posicion.
                     * En caso de necesitar tracking de posicion, en este punto se debe el iniciar el SW de trackeo **/
                    FarmaciaAdultMayor.this.listener.onLocationChanged(location);
                }

                @Override
                public void onStatusChanged(String provider, int status, Bundle extras) {

                }

                @Override
                public void onProviderEnabled(String provider) {

                }

                @Override
                public void onProviderDisabled(String provider) {

                }
            });

            /** el mapa es consciente de la posción, pero necesitamos entregarle
             * la primera posicion al mapa para que cambie la vista entregada, siempre
             * y cuando, el telefono haya registrado su posicion antes. **/


        }
        mCurrentLocation = getLastKnownLocation();
        //Llamada al método que busca las farmacias de turno
        serviceFarmacias();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        /** iniciamos el proceso de captura de posiciones **/
        enableMyLocation();
    }

    /** se inicializa el evento que captura las posiciones para el mapa **/
    @Override
    public void activate(LocationSource.OnLocationChangedListener onLocationChangedListener) {
        this.listener = onLocationChangedListener;
    }

    @Override
    public void deactivate() {
        this.listener = null;
    }

    private Location getLastKnownLocation() {
        mLocationManager = (LocationManager)getApplicationContext().getSystemService(LOCATION_SERVICE);
        List<String> providers = mLocationManager.getProviders(true);
        Location bestLocation = null;
        for (String provider : providers) {
            Location l = mLocationManager.getLastKnownLocation(provider);
            if (l == null) {
                continue;
            }
            if (bestLocation == null || l.getAccuracy() < bestLocation.getAccuracy()) {
                // Found best last known location: %s", l);
                bestLocation = l;
            }
        }
        return bestLocation;
    }
}


