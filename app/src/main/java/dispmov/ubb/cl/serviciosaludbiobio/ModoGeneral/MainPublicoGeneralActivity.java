package dispmov.ubb.cl.serviciosaludbiobio.ModoGeneral;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;

import dispmov.ubb.cl.serviciosaludbiobio.DialogCustom;
import dispmov.ubb.cl.serviciosaludbiobio.ModoAdultoMayor.MainAdultoMayorActivity;
import dispmov.ubb.cl.serviciosaludbiobio.Navigation.NavigationActivity;
import dispmov.ubb.cl.serviciosaludbiobio.R;

public class MainPublicoGeneralActivity extends NavigationActivity implements  DialogCustom.ListenerButton{

    private final static int ID_DIALOG_SALIR = 0;
    private DialogCustom dlog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_publico_general);
        setContext(this);
    }
    @Override
    public void onBackPressed(){
        dlog = new DialogCustom();
        dlog.setIdDialog(ID_DIALOG_SALIR);
        dlog.setTxt_msg("¿Está seguro de que desea salir de la aplicación?");
        dlog.setTxt_title("Servicio de Salud Bio-Bio");
        dlog.setTxt_btn_neg("Cancelar");
        dlog.setTxt_btn_pos("Salir");
        dlog.show(getFragmentManager(),"My Dialog Custom");
    }
    @Override
    public void onClickNegButton(int idDialog) {
        dlog.dismiss();
    }

    @Override
    public void onClickPosButton(int idDialog) {
        switch (idDialog) {
            case ID_DIALOG_SALIR:
                finish();
                break;
        }
    }
}
