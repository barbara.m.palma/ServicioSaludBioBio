package dispmov.ubb.cl.serviciosaludbiobio.ModoGeneral;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.ImageButton;

import dispmov.ubb.cl.serviciosaludbiobio.Navigation.NavigationActivity;
import dispmov.ubb.cl.serviciosaludbiobio.R;

public class Contacto extends NavigationActivity {

    private ImageButton iconfacebook;
    private ImageButton icontwitter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contacto);
        setContext(this);

        iconfacebook = (ImageButton) findViewById(R.id.iconfacebook);
        iconfacebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri uri = Uri.parse("https://www.facebook.com/ministeriosaludchile/");
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
            }
        });
        icontwitter = (ImageButton) findViewById(R.id.icontwitter);
        icontwitter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri uri = Uri.parse("https://twitter.com/ministeriosalud?lang=es");
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
            }
        });
    }
    @Override
    public void onBackPressed(){
        Intent iniciar = new Intent(getApplicationContext(), MainPublicoGeneralActivity.class);
        startActivity(iniciar);
        finish();
    }
}
