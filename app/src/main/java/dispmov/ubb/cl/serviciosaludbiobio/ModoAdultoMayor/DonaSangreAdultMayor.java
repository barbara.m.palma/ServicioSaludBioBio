package dispmov.ubb.cl.serviciosaludbiobio.ModoAdultoMayor;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import dispmov.ubb.cl.serviciosaludbiobio.R;

public class DonaSangreAdultMayor extends AppCompatActivity {

    Button retroceder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dona_sangre__adult_mayor);

         retroceder = findViewById(R.id.retroceder);
        retroceder.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent iniciar = new Intent(getApplicationContext(), MainAdultoMayorActivity.class);
                startActivity(iniciar);
                finish();
            }
        });
    }
    @Override
    public void onBackPressed(){
        Intent iniciar = new Intent(getApplicationContext(), MainAdultoMayorActivity.class);
        startActivity(iniciar);
        finish();
    }
}
